import socket
import pyaudio
import wave
import sys

UDP_IP = "localhost"
UDP_PORT = 5005

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 5

if sys.platform == 'darwin':
    CHANNELS = 1

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                output=True,
                frames_per_buffer=CHUNK)


import select, socket

port = 5005  # where do you expect to get a msg?
bufferSize = 1024 # whatever you need

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(('', port))
s.setblocking(0)

#sock = socket.socket(socket.AF_INET, # Internet
#                     socket.SOCK_DGRAM) # UDP

#sock.bind((UDP_IP, UDP_PORT))

wf = wave.open(sys.stdout, 'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(RATE)

stream.start_stream()

while stream.is_active():
    result = select.select([s],[],[])
    data = result[0][0].recv(CHUNK * CHANNELS * 2)
    #wf.writeframes(data)
    stream.write(data, CHUNK)

stream.stop_stream()
stream.close()

wf.close()
