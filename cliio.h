#ifndef CLIIO_H
#define CLIIO_H

#include <QObject>
#include <QAudio>
#include <QAudioInput>
#include <QIODevice>
#include <QProcess>
class CliIO : public QObject
{
    Q_OBJECT
public:
    explicit CliIO(QObject *parent = 0);
    ~CliIO();
private:

    QProcess* p;
    QProcess* p2;

public slots:
    void runCMD(QString cmd);
    void killServer();
};

#endif // CLIIO_H
