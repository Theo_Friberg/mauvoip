#include "cliio.h"
#include <QDebug>
#include <QProcess>

#define is_debug true

CliIO::CliIO(QObject *parent)
{
    this->p = NULL;
    this->p2 = NULL;
    QProcess* process = new QProcess();
    if(!is_debug){
        process->start("python \"udp-client.py\"");
    }else{
        process->start("python \"/home/theo/audiopipe-python/bin 2.x/udp-client.py\"");
    }
    this->p2 = process;
}

void CliIO::runCMD(QString cmd){

    QProcess* process = new QProcess();
    if(!is_debug){
        process->start("python \"udp-system.py\" "+cmd);
    }else{
        process->start("python \"/home/theo/audiopipe-python/bin 2.x/udp-system.py\" "+cmd);
    }
    this->p = process;


}
void CliIO::killServer(){
    if(this->p != NULL)
    this->p->close();
}

CliIO::~CliIO(){

    this->killServer();
    if(this->p2 != NULL)
    this->p2->close();

}
