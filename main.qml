import QtQuick 2.0
import QtQuick.Window 2.1
import QtQuick.Controls 1.0
import QtGraphicalEffects 1.0
import com.udpvoip.CliIO 1.0

Window {
    id: window1
    width: 1920
    height: 1080
    visible: true
    color: "#00e8b1"

    CliIO{
        id: cli
        property bool connected: false;
    }

    TextField {
        id: textField1
        x: 748
        y: 564
        width: parent.width / 3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        placeholderText: qsTr("IP here")
    }

    Image{
        source: "datagram4.png"
        anchors.horizontalCenter: parent.horizontalCenter;
        width: parent.width < parent.height - info.y ? parent.width /2: info.y;
        height: parent.width < parent.height - info.y ? parent.width /2: info.y;
        sourceSize.width: width;
        sourceSize.height: height;
    }

    Text {
        id: info
        x: 864
        y: 560
        text: qsTr("Input IP  and click the icon below to start call.")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 184
        anchors.horizontalCenterOffset: 1
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 20
        font.weight: font.bold;
        font.family: "Roboto Slab"
        color: "white";
    }
    Glow {
        anchors.fill: info
        radius: 8
        samples: 16
        color: "black"
        source: info
    }

    MouseArea {
        id: button1
        width: 70;
        height: 70;
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        onClicked: {
            cli.connected = !cli.connected;
            if(cli.connected){
                cli.runCMD(textField1.text);
                flipable.state="back"
            }else{
                cli.killServer();
                flipable.state="front"
            }


        }
        Flipable {
            id: flipable
            anchors.fill: parent

            property bool flipped: false

            front:Image{
                anchors.fill: parent;
                sourceSize.width: width;
                sourceSize.height: height;
                source: "call.png"
            }
            back:Image{
                anchors.fill: parent;
                sourceSize.width: width;
                sourceSize.height: height;
                source: "hang_up.png"
                states: State {
                    name: "back"
                    PropertyChanges { target: rotation; angle: 180 }
                    when: flipable.flipped
                }

                transitions: Transition {
                    NumberAnimation { target: rotation; property: "angle"; duration: 200; easing: easing.InOutQuint }
                }
            }

            transform: Rotation {
                id: rotation
                origin.x: flipable.width/2
                origin.y: flipable.height/2
                axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
                angle: 0    // the default angle
            }

            states: State {
                name: "back"
                PropertyChanges { target: rotation; angle: 180 }
                when: flipable.flipped
            }

            transitions: Transition {
                NumberAnimation { target: rotation; property: "angle"; duration: 200; easing: easing.InOutQuint }
            }
        }

    }

}
