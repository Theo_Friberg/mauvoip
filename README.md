# README #

This application requires Python 2.7x, Qt 5.4 and PyAudio to run. Should work on most Linux distributions and Mac OSX. Windows support coming soon. Encryption coming soon.

###What's this###

MauVoip is an app for peer-to-peer voice calls over UDP

### Installation ###

####Installation under 64 bit debian or related####

If you are running a 64 bit or 32 bit debian-based distro, you can grap the deb package from the downloads-section on the left. Use at your own risk, briefly tested (install, launch, call, quit, uninstall) on the following:

- Ubuntu 64 & 32 bit (LXDE, GNOME, KDE)
- Linux Mint 32 bit (Cinnamon)

####Compiling from source####

Would you not be able to install the binary, want to compile it, use another os or simply not want to risk installing it with superuser privileges, you can compile the app from source.

You must install Python 2.7x from the official site. Then, you can download PyAudio from its official site (they distribute platform-specific binaries). Then build the app using QMake. After building it, copy udp-system.py and udp-client.py from the sources to the directory you built the app in.

### Contact me ###

You can find me at theo.friberg@gmail.com