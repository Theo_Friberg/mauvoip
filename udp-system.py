import socket, sys

UDP_IP = sys.argv[1]#"10.0.1.10"
UDP_PORT = 5005
MESSAGE = b"Hello, World!"

#!/usr/bin/python3

"""
PyAudio example: Record a few seconds of audio and save to a WAVE
file.
"""

import pyaudio
import wave
import sys

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 5

if sys.platform == 'darwin':
    CHANNELS = 1

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

#print("* recording")

wf = wave.open(sys.stdout, 'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(RATE)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

while True:
    data = stream.read(CHUNK)
    #frames.append(data)
    #wf.writeframes(data)
    sock.sendto(data, (UDP_IP, UDP_PORT))
    #sys.stdout.buffer.write(data)

wf.close()
#print("* done recording")
stream.stop_stream()
stream.close()
p.terminate()
