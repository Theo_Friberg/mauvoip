TEMPLATE = app

QT += qml quick multimedia

SOURCES += main.cpp \
    cliio.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    cliio.h

DISTFILES += \
    udp-system.py \
    ../datagram4.png
